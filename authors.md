# Authors

## For article writers available the following [Creative Commons](https://creativecommons.org/) licenses:

### CC BY

This license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, so long as attribution is given to the creator. The license allows for commercial use.

```yaml
license:
  - by
```

### CC BY-SA

This license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, so long as attribution is given to the creator. The license allows for commercial use. If you remix, adapt, or build upon the material, you must license the modified material under identical terms.

```yaml
license:
  - by
  - sa
```

### CC BY-NC

This license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format for noncommercial purposes only, and only so long as attribution is given to the creator.

```yaml
license:
  - by
  - nc
```

### CC BY-NC-SA

This license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format for noncommercial purposes only, and only so long as attribution is given to the creator. If you remix, adapt, or build upon the material, you must license the modified material under identical terms.

```yaml
license:
  - by
  - nc
  - sa
```

### CC BY-ND

This license allows reusers to copy and distribute the material in any medium or format in unadapted form only, and only so long as attribution is given to the creator. The license allows for commercial use.

```yaml
license:
  - by
  - nd
```

### CC BY-NC-ND

This license allows reusers to copy and distribute the material in any medium or format in unadapted form only, for noncommercial purposes only, and only so long as attribution is given to the creator.

```yaml
license:
  - by
  - nc
  - nd
```

### CC 0

(aka CC Zero) is a public dedication tool, which allows creators to give up their copyright and put their works into the worldwide public domain. CC0 allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, with no conditions.

```yaml
license:
  - zero
```
