import type { Config } from 'tailwindcss'
import defaultTheme from 'tailwindcss/defaultTheme'
import daisyui from 'daisyui'
import typography from '@tailwindcss/typography'

export default <Partial<Config>>{
  darkMode: 'class',
  theme: {
    extend: {
      fontFamily: {
        sans: ['Montserrat', ...defaultTheme.fontFamily.sans],
        mono: ['"Fira Code"', ...defaultTheme.fontFamily.mono],
      },
    },
  },
  plugins: [daisyui, typography],
  daisyui: {
    themes: [
      {
        dark: {
          primary: '#00dc82',
          secondary: '#746CF7',
          accent: '#EA4C3C',
          neutral: '#18181B',
          'base-100': '#0c0c0d',
          'base-200': '#18181b',
          info: '#A4B6FC',
          success: '#50FA7B',
          warning: '#F1FA8C',
          error: '#FF5555',
        },
        light: {
          primary: '#00dc82',
          secondary: '#746CF7',
          accent: '#EA4C3C',
          neutral: '#18181B',
          'base-100': '#F3F3F7',
          'base-200': '#F2F2F2',
          info: '#A4B6FC',
          success: '#50FA7B',
          warning: '#F1FA8C',
          error: '#FF5555',
        },
      },
    ],
  },
}
