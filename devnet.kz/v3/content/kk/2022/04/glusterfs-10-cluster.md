---
title: Ubuntu серверінде 22.04 (Jammy) Glusterfs 10 кластерін орналастыру
short: Ubuntu жүйесіндегі Glusterfs 10 кластері (Jammy)
description: Glusterfs - Kubernetes / Docker тобын басқару жүйелерінде сақтау ретінде пайдалануға болатын бөлінген файлдық жүйе
createdAt: 2022-04-06
category: devops
isDraft: false
slug: glusterfs-10-cluster
author: Yerlen Zhubangaliyev
email: yerlen@yerlen.com
pin: false
buymeacoffee: https://www.buymeacoffee.com/teamxg
linkedin: https://www.linkedin.com/in/yerlen-zhubangaliyev/
gitlab: https://gitlab.com/devnet.kz/
tags:
  - devops
  - glusterfs
  - linux
  - storage
image: /images/covers/glusterfs-logo.svg
license:
  - by
  - nc
language:
  - ru
  - en
  - kk
---

Glusterfs распределенная файловая система, которую можете использовать как хранилище в системах оркестрации Kubernetes / Docker swarm.
Позволяет построить отказоустойчивое и масштабируемое хранилище для ваших приложений.

Что мы имеем:

- 3 виртуальных машины (192.168.1.11/24, 192.168.1.12/24, 192.168.1.13/24), которые будем использовать как серверный кластер хранилище
- 1 виртуальную машину (192.168.1.14/24), как клиент кластера

## Серверы

Начнем с установки сервера glusterfs на серверах кластера

```bash
sudo apt update && sudo apt install glusterfs-server -y
sudo systemctl start glusterd.service
sudo systemctl enable glusterd.service
```

Отредактируйте `/etc/hosts` на всех виртуальных машинах

```bash,[/etc/hosts]
192.168.1.11    glusterfs-server-01
192.168.1.12    glusterfs-server-02
192.168.1.13    glusterfs-server-03
```

На первом сервере добавим 2 других в кластер выполнив следующее:

```bash
gluster peer probe glusterfs-server-02
gluster peer probe glusterfs-server-03
```

Проверим

```bash
gluster peer probe status
```

Должно выйти

```bash
Number of Peers: 2

Hostname: glusterfs-server-02
Uuid: bb5e609b-d99d-4d49-ae2f-af0879df7588
State: Peer in Cluster (Connected)

Hostname: glusterfs-server-03
Uuid: 88315bca-1b12-4aa2-acb3-8bcc812c85b0
State: Peer in Cluster (Connected)
```

На первом сервере добавим VOLUME из 3 реплик кластера

```bash
export VOLUME_NAME="my-volume"
export VOLUME_PATH="/glusterfs-storage"
gluster volume create $VOLUME_NAME transport tcp replica 3 glusterfs-server-01:$VOLUME_PATH glusterfs-server-02:$VOLUME_PATH glusterfs-server-03:$VOLUME_PATH force
```

И запустим VOLUME

```bash
gluster volume start $VOLUME_NAME
```

Проверим статус

```bash
gluster volume status
```

Выведет это

```bash
Status of volume: my-volume
Gluster process                              TCP Port  RDMA Port  Online  Pid
-------------------------------------------------------------------------------
Brick glusterfs-server-01:/glusterfs-storage 58198     0          Y       972
Brick glusterfs-server-02:/glusterfs-storage 51841     0          Y       931
Brick glusterfs-server-03:/glusterfs-storage 52348     0          Y       919
Self-heal Daemon on localhost                N/A       N/A        Y       989
Self-heal Daemon on glusterfs-server-03      N/A       N/A        Y       936
Self-heal Daemon on glusterfs-server-02      N/A       N/A        Y       949

Task Status of Volume my-volume
-------------------------------------------------------------------------------
There are no active volume tasks
```

## Клиент

На клиентской машине поставим

```bash
sudo apt update && sudo apt install glusterfs-client
mkdir -p /storage
```

Примонтируем файловую систему

```bash
mount -t glusterfs glusterfs-server-01:/my-volume /storage
```

Добавьте в конец файла `/etc/fstab` строчку ниже, для перманентного подключения и выполните команду `sudo reboot`

```bash,[/etc/fstab]
glusterfs-server-01:/my-volume /storage glusterfs defaults,_netdev 0 0
```

Теперь можно проверить

```bash
touch /storage/test.txt
```

И на серверах, что реплики работают

```bash
ls -la /glusterfs-storage

total 24
drwxr-xr-x   4 root root 4096 Apr  6 15:15 .
drwxr-xr-x  20 root root 4096 Apr  6 14:33 ..
drw------- 262 root root 4096 Apr  6 14:39 .glusterfs
drwxr-xr-x   2 root root 4096 Apr  6 14:39 .glusterfs-anonymous-inode-f1845067-9924-4985-8a5b-a2063b186060
-rw-r--r--   2 root root    0 Apr  6 15:15 test.txt
```
