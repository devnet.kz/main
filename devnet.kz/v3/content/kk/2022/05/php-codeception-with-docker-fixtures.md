---
title: Docker контейнерінен Codeception жүйесі арқылы BDD сынақтарын іске қосу (2-бөлім)
short: Codeception BDD сынақтарын Docker контейнерінен іске қосыңыз
description: BDD сынақтарын кодцепциялық жүйеде жазу өте ыңғайлы. Фреймворк әбден жетілген және тұрақты.
createdAt: 2022-05-01
category: codeception
isDraft: true
slug: run-dockerized-codeception-bdd-tests
author: Yerlen Zhubangaliyev
email: yerlen@yerlen.com
pin: false
buymeacoffee: https://www.buymeacoffee.com/teamxg
linkedin: https://www.linkedin.com/in/yerlen-zhubangaliyev/
gitlab: https://gitlab.com/devnet.kz/
tags:
  - php
  - codeception
  - docker
image: /images/covers/codeception-logo.svg
license:
  - by
  - nc
language:
  - ru
  - kk
---

Писать BDD тесты на фреймворке codeception довольно удобно. Фреймворк довольно взрослый и стабильный.
