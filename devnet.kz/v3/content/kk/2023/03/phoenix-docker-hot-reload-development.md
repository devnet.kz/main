---
title: Phoenix-ті Docker ортасында ыстық қайта жүктеу арқылы әзірлеу 🔥
short: Phoenix-ті Docker ортасында ыстық қайта жүктеу арқылы әзірлеу 🔥
description: Docker ортасында Phoenix-ті әзірлеуі ыстық қайта жүктеу арқылы 🔥
createdAt: 2023-03-06
updatedAt: 2023-03-06
category: elixir
isDraft: false
slug: phoenix-docker-hot-reload-development
author: Amirzhan Aliyev
email: letavocado@proton.me
pin: false
buymeacoffee: https://www.buymeacoffee.com/teamxg
linkedin: https://www.linkedin.com/in/yerlen-zhubangaliyev/
gitlab:
tags:
  - elixir
  - phoenix
  - docker
  - docker-compose
  - hot-reload
image: /images/covers/elixir+doker+hot-reload-preview.jpg
license:
  - by
  - nc
language:
  - en
  - ru
  - kk
---

# {{title}}

![alt text](/images/covers/elixir+doker+hot-reload-preview.jpg)

# Phoenix дегеніміз не?

[Phoenix](https://www.phoenixframework.org/) — [Elixir](https://elixir-lang.org/) функционалдық бағдарламалау тілінде жасалған веб-құрылым. Elixir, өз кезегінде, [BEAM](https://www.erlang.org/blog/a-brief-beam-primer/) виртуалды машинасын және [OTP(Open Telecom Platform)](https ://ru.wikipedia.org/wiki/Open_Telecom_Platform) пайдаланатын Erlang негізінде жасалған. Phoenix нақты уақыттағы веб-қосымшаларды құру үшін қолайлы, ол аз кідіріспен бөлінген жүйелер үшін ақауларға төзімділікті қамтамасыз етеді.

# Phoenix-ті орнату

Phoenix-ті орнату бойынша ең жақсы нұсқаулар ресми сайтта [ресми нұсқаулық](https://hexdocs.pm/phoenix/up_and_running.html).

# Docker ортасында әзірлеу

## Dockerfile файлын жасаy

1. Жобаның негізінде [ресми elixir суреті](https://hub.docker.com/_/elixir) негізінде құрылған Dockerfile жасау керек, осылайша Elixir және Erlang үшін әзірлеу орталарын орнату мәселесі шешіледі

```docker
FROM elixir:latest
```

2. WORKDIR — жұмыс каталогын орнату

```docker
WORKDIR /app
```

3. Жұмыс каталогына қажетті файлдарды импорттау

```docker
ADD . /app
```

4. Кескінді жаңартыңыз және келесі бумаларды орнатыңыз:

- `npm` - js тәуелділіктерімен жұмыс істеу үшін қажет
- `build-essential` - компиляцияға қажет пакеттер
- `inotify-tools` - Live Reloading Phoenix жүйесіне енгізілген. Сервердегі код пен ресурстарды (heex, ex, exs, css, js, кескіндер, т.б.) өзгерткен кезде браузердегі бетті автоматты түрде жаңартады. Бұл мүмкіндік жұмыс істеуі үшін сізге (файлдық жүйе) бақылаушы қажет[https://github.com/inotify-tools/inotify-tools/wiki].

```docker
RUN apt-get update && apt-get -y install npm build-essential inotify-tools
```

5. `assets` каталогында js тәуелділіктерін орнатыңыз

```docker
RUN npm install --prefix ./assets
```

6. `hex` және `rebar` жергілікті көшірмелерін орнатыңыз

- `hex` - Erlang экожүйесіне арналған пакет менеджері
- `rebar` - жобаны ыңғайлы басқару құралы
- `--force` - қабықша шақырусыз мәжбүрлі орнату; негізінен `make` сияқты құрастыру жүйелеріндегі автоматтандыруға арналған

```docker
RUN mix local.hex --force && mix local.rebar --force
```

7. Тәуелділіктерді орнату және құрастыру

```docker
RUN mix do deps.get, compile
```

8. 4000 портын ашу

```docker
EXPOSE 4000
```

9. Жобаның түбіріндегі `bash`-сценарийін іске қосу

```docker
CMD ["./dev"]
```

Нәтижедегі Dockerfile

```docker
FROM elixir:latest

WORKDIR /app

ADD . /app

RUN apt-get update && apt-get -y install npm build-essential inotify-tools
RUN npm install --prefix ./assets
RUN mix local.hex --force && mix local.rebar --force
RUN mix do deps.get, compile

EXPOSE 4000

CMD ["./dev"]
```

## Жобаны іске қосу cкрипті

Жоба құрастырылғаннан кейін төмендегі сценарийді іске қосу арқылы жобаның ішінде `iex` сеансын бастауға болады. `-S mix` интерактивті қабықшада жұмыс істеу үшін қажет

```bash
#!/bin/sh
exec iex -S mix phx.server
```

## docker-compose.yml жасау

```yaml
version: '3.9' # docker-compose бағдарламасының соңғы нұсқасын пайдаланыңыз
services:
  phoenix-app:
    container_name: phoenix-container
    restart: always

    build: . # Dockerfile жолын көрсетіңіз, біздің жағдайда ол жобаның түбірінде

    environment:
      - MIX_ENV=dev # mix ортасы үшін айнымалы мәндерді орнату

    # кодтағы өзгерістерге жауап беретін қажетті файлдарды орнатыңыз. Қайта ыстық жүктеу жұмыс істеуі үшін қажет
    volumes:
      - ./assets:/app/assets
      - ./priv:/app/priv
      - ./lib:/app/lib
      - ./config:/app/config
    ports:
      - '4000:4000'

    # `tty: true` және `stdin_open: true` интерактивті қабық үшін қажет. Онсыз iex қате жібереді
    tty: true
    stdin_open: true
```

## Қорытынды

Бұл мақалада `Phoenix` жобаларын `Docker` ортасында ыстық қайта жүктеу арқылы қалай әзірлеу керектігі көрсетілген.
