---
title: Запуск BDD тестов через фреймворк Codeception из Docker контейнера (часть 2)
short: Запуск Codeception BDD тестов из Docker контейнера
description: Писать BDD тесты на фреймворке codeception довольно удобно. Фреймворк довольно взрослый и стабильный.
createdAt: 2022-05-01
category: codeception
isDraft: true
slug: run-dockerized-codeception-bdd-tests
author: Yerlen Zhubangaliyev
email: yerlen@yerlen.com
pin: false
buymeacoffee: https://www.buymeacoffee.com/teamxg
linkedin: https://www.linkedin.com/in/yerlen-zhubangaliyev/
gitlab: https://gitlab.com/devnet.kz/
tags:
- php
- codeception
- docker
image: /images/covers/codeception-logo.svg
license:
- by
- nc
language:
- ru
---

Писать BDD тесты на фреймворке codeception довольно удобно. Фреймворк довольно взрослый и стабильный.

