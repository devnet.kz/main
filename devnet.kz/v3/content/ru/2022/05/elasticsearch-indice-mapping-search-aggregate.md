---
title: Elasticsearch индекс, маппинг, поиск и агрегация (часть 1)
short: Elasticsearch индекс, маппинг, поиск и агрегация (часть 1)
description: 
createdAt: 2022-05-02
category: elasticsearch
isDraft: true
slug: elasticsearch-indice-mapping-search-aggregate
author: Yerlen Zhubangaliyev
email: yerlen@yerlen.com
pin: false
buymeacoffee: https://www.buymeacoffee.com/teamxg
linkedin: https://www.linkedin.com/in/yerlen-zhubangaliyev/
gitlab:
tags:
- elasticsearch
image: /images/covers/elasticsearch-logo.svg
license:
- by
- nc
language:
- ru
---

## 
