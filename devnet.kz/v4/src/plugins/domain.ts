declare module '#app' {
    interface NuxtApp {
        $layout(): string
    }
}

declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $layout(): string
    }
}

import _ from "lodash-es"

export default defineNuxtPlugin(() => {
    const url: URL      = useRequestURL(),
          runtimeConfig = useRuntimeConfig()

    return {
        provide: {
            layout: (): String => {
                let allowedDomains = runtimeConfig
                        .public
                        .allowedDomains
                        .split(','),
                    defaultDomain  = runtimeConfig
                        .public
                        .defaultDomain,
                    domainMap      = () => {
                        return _.fromPairs(runtimeConfig
                            .public
                            .domainMap
                            .split(',')
                            .map(s => s.split(':'))
                        )
                    }

                if (allowedDomains?.includes(url.hostname)) {
                    return (url.hostname) ? String(domainMap()[url.hostname]) : ''
                } else {
                    return (defaultDomain) ? String(domainMap()[defaultDomain]) : ''
                }
            }
        }
    }
})
