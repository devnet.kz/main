import contentRoutes from "./src/plugins/contentRoutes";

export default {
    dev: process.env.NODE_ENV !== 'production',

    server: {
        port:   3000,
        host:   '0.0.0.0',
        timing: false
    },
    // Global page headers: https://go.nuxtjs.dev/config-head
    head:         {
        title:     process.env.BASE_DOMAIN,
        htmlAttrs: {
            lang: 'en',
        },
        meta:      [
            {
                charset: 'utf-8'
            },
            {
                name:    'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid:     'description',
                name:    'description',
                content: ''
            },
            {
                name:    'format-detection',
                content: 'telephone=no'
            }
        ],
    },
    render:       {
        csp:   {
            reportOnly:    false,
            hashAlgorithm: 'sha256',
            policies:      {
                'default-src':     ["'self'"],
                'img-src':         ["'self'", 'https:', '*.google-analytics.com'],
                'worker-src':      ["'self'", `blob:`],
                'child-src':       ["'self'", `blob:`],
                'style-src':       ["'self'", "'unsafe-inline'"],
                'script-src':      ["'self'", '*.google-analytics.com', '*.devnet.kz'],
                'connect-src':     ["'self'", "ws://" + process.env.SELF_HOST, '*.google-analytics.com'],
                'form-action':     ["'self'"],
                'frame-ancestors': ["*.devnet.kz"],
                'frame-src':       ["'self'", "*.devnet.kz"],
                'object-src':      ["'none'"],
                'base-uri':        [],
            },
        },
        http2: {
            push:       true,
            pushAssets: (
                            req,
                            res,
                            publicPath,
                            preloadFiles
                        ) =>
                            preloadFiles.map(
                                (f) =>
                                    `<${publicPath}${f.file}>; rel=preload; as=${f.asType}`
                            ),
        },
    },
    buildModules: [
        '@nuxtjs/tailwindcss',
        '@nuxtjs/google-fonts',
        '@nuxtjs/google-analytics',
        '@nuxtjs/moment',
        '@nuxtjs/pwa',
    ],
    modules:      [
        /** @see https://github.com/victor-perez/nuxt-helmet#readme */
        'nuxt-helmet',
        /** @nuxtjs/svg */
        '@nuxtjs/svg',
        /** @nuxtjs/i18n */
        '@nuxtjs/i18n',
        /** @nuxt/content */
        '@nuxt/content',
        /** @nuxtjs/sitemap */
        '@nuxtjs/sitemap',
    ],
    components:   true,
    srcDir:       "src/",
    css:          [
        '~/assets/css/fonts.css',
    ],
    scripts:      {
        "build:modern": "nuxt build --modern=server",
        "start:modern": "nuxt start --modern=server"
    },
    purgeCSS:     {
        whitelist: ['dark'],
    },
    hooks:        {
        'content:file:beforeInsert': (document) => {
            if (document.extension === '.md') {
                const {minutes, time, words} = require('reading-time')(document.text)

                document.readingTime = {minutes, time, words}
            }
        }
    },
    plugins:      [
        '~/plugins/mixins.js',
        '~/plugins/moment.js',
        '~/plugins/domain.server.js',
    ],
    /**
     * @see https://nuxtjs.org/docs/configuration-glossary/configuration-loading-indicator
     */
    loadingIndicator: {
        color: '#407634',
    },
    /******** Modules configuration ********/
    // Tailwindcss
    tailwindcss:     {
        cssPath:        '~/assets/css/tailwind.css',
        configPath:     '~/configs/tailwind.config.js',
        exposeConfig:   false,
        config:         {},
        injectPosition: 0,
        viewer:         false
    },
    i18n:            {
        langDir:       '~/locales/',
        defaultLocale: 'ru',
        strategy:      'prefix',
        parsePages:    true,
        vueI18nLoader: true,
        baseUrl:       process.env.BASE_URL,
        // noPrefixDefaultLocale: false,
        locales:            [
            {
                code: 'ru',
                iso:  'ru-RU',
                file: 'ru.js',
                dir:  'ltr',
                name: 'Русский',
            },
            {
                code: 'en',
                iso:  'en-US',
                file: 'en.js',
                dir:  'ltr',
                name: 'English'
            },
            // {
            //     code: 'kk',
            //     iso:  'ru-RU',
            //     file: 'kk.js',
            //     dir:  'ltr',
            //     name: 'Қазақ'
            // },
        ],
        vueI18n:            {
            fallbackLocale: 'ru',
        },
        pluralizationRules: {
            /**
             * @param choice {number} a choice index given by the input to $tc: `$tc('path.to.rule', choiceIndex)`
             * @param choicesLength {number} an overall amount of available choices
             * @returns a final choice index to select plural word by
             */
            'ru': function (choice, choicesLength) {
                // this === VueI18n instance, so the locale property also exists here

                if (choice === 0) {
                    return 0;
                }

                const teen = choice > 10 && choice < 20;
                const endsWithOne = choice % 10 === 1;

                if (choicesLength < 4) {
                    return (!teen && endsWithOne) ? 1 : 2;
                }

                if (!teen && endsWithOne) {
                    return 1;
                }

                if (!teen && choice % 10 >= 2 && choice % 10 <= 4) {
                    return 2;
                }

                return (choicesLength < 4) ? 2 : 3;
            }
        }
    },
    content:         {
        apiPrefix: 'api-content',
        dir:       'contents',
        liveEdit:  false,
    },
    googleAnalytics: {
        id: process.env.GOOGLE_ANALYTICS_ID
    },
    googleFonts:     {
        display:    'swap',
        prefetch:   true,
        preconnect: true,
        preload:    true,
        download:   true,
        fontsDir:   'fonts',
        stylePath:  'css/fonts.css',
        subsets:    ['cyrillic', 'latin'],
        families:   {
            'Roboto Condensed': {
                wght: [100, 400],
                ital: [300, 400]
            },
        },
    },
    moment:          {
        locales:       ['ru'],
        defaultLocale: 'ru',
    },
    sitemap:         {
        hostname: process.env.BASE_URL,
        routes() {
            return contentRoutes();
        },
        defaults: {
            changefreq: 'daily',
            priority:   1,
            lastmod:    new Date()
        },
    },
    pwa:             {
        meta: {
            ogType:     "website",
            ogSiteName: process.env.BASE_DOMAIN,
            ogHost:     process.env.BASE_URL,
        },
    },
}
