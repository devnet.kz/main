import Vue from 'vue'

const mixin = {
    methods: {
        truncate (string, value) {
            return (string || '').substring(0, value) + (string.length > value) ? '…' : ''
        },
        pageTitle (...values) {
            return values.join(' | ')
        },
        gitContentUrl (path) {
            return "https://gitlab.com/devnet.kz/site/-/tree/main/devnet.kz/v2/src/contents" + path + '.md'
        },
    }
}

Vue.mixin(mixin);
