export default (ctx, inject) => {
    inject('hostname', ctx.req.headers['host'])
}
