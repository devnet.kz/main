import STATUS_TYPES from '~/configs/status'
import {parsesJSON} from '~/utils'

const {INIT, SUCCESS} = STATUS_TYPES

const state = () => ({
    articles:        {
        status: INIT,
        error:  null,
        data:   []
    },
    tags:            {
        status: INIT,
        error:  null,
        data:   []
    },
    authors:         {
        status: INIT,
        error:  null,
        data:   []
    },
    article:         {
        status: INIT,
        error:  null,
        data:   []
    },
    articleSurround: {
        status: INIT,
        error:  null,
        data:   []
    },
})

const mutations = {
    GET_ARTICLES(state, payload) {
        state.articles.data = payload
        state.articles.status = SUCCESS
    },
    GET_TAGS(state, payload) {
        state.tags.data = payload
        state.tags.status = SUCCESS
    },
    GET_AUTHORS(state, payload) {
        state.authors.data = payload
        state.authors.status = SUCCESS
    },
    GET_ARTICLE(state, payload) {
        state.article.data = payload
        state.article.status = SUCCESS
    },
    GET_ARTICLE_SURROUND(state, payload) {
        state.articleSurround.data = payload
        state.articleSurround.status = SUCCESS
    },
}

const getters = {
    getArticles:        (state) => parsesJSON(state.articles),
    getTags:            (state) => parsesJSON(state.tags),
    getAuthors:         (state) => parsesJSON(state.authors),
    getArticle:         (state) => parsesJSON(state.article),
    getArticleSurround: (state) => parsesJSON(state.articleSurround),
}

const actions = {
    async getArticles({commit}, params, callback) {
        const storeArticles = await this.$content(this.$i18n.locale, {deep: true, body: false})
            .only(
                [
                    'short',
                    'title',
                    'description',
                    'category',
                    'createdAt',
                    'updatedAt',
                    'readingTime',
                    'path',
                    'tags',
                    'image',
                    'isDraft',
                    'language',
                ]
            )
            .sortBy('isDraft')
            .sortBy('createdAt', 'desc')
            .fetch()

        commit('GET_ARTICLES', storeArticles)
    },
    async getTags({commit}, params, callback) {
        const storeTags = await this.$content(this.$i18n.locale, {deep: true, body: false})
            .only(
                [
                    'tags',
                ]
            )
            .sortBy('createdAt', 'desc')
            .where(
                {
                    isDraft: {$eq: false}
                }
            )
            .fetch()

        commit('GET_TAGS', storeTags)
    },
    async getAuthor({commit}, params, callback) {
        const storeAuthors = await this.$content({deep: true, body: false})
            .only(
                [
                    'author',
                ]
            )
            .fetch()

        commit('GET_AUTHORS', storeAuthors)
    },
    async getArticle({commit}, params, callback) {
        const {year, month, slug} = params
        const storeArticle = await this.$content(this.$i18n.locale, year, month, slug)
            .where(
                {
                    isDraft: {$eq: false}
                }
            )
            .fetch()

        commit('GET_ARTICLE', storeArticle)
    },
    async getArticleSurround({commit}, params, callback) {
        const storeArticleSurround = await this.$content(this.$i18n.locale, {deep: true})
            .where(
                {
                    isDraft: {$eq: false}
                }
            )
            .sortBy('createdAt', 'asc')
            .only(["title", "path"])
            .surround(this.$router.currentRoute.path, {before: 1, after: 1})
            .fetch()

        commit('GET_ARTICLE_SURROUND', storeArticleSurround)
    },
}

export default {
    state,
    mutations,
    getters,
    actions
}
