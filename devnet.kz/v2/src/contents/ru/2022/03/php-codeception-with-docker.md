---
title: Запуск BDD тестов через фреймворк Codeception из Docker контейнера (часть 1)
short: Запуск Codeception BDD тестов из Docker контейнера
description: Писать BDD тесты на фреймворке codeception довольно удобно. Фреймворк довольно взрослый и стабильный.
createdAt: 2022-03-27
category: codeception
isDraft: false
slug: php-codeception-with-docker
author: Yerlen Zhubangaliyev
email: yerlen@yerlen.com
pin: false
buymeacoffee: https://www.buymeacoffee.com/teamxg
linkedin: https://www.linkedin.com/in/yerlen-zhubangaliyev/
gitlab: https://gitlab.com/devnet.kz/
tags:
- php
- codeception
- docker
image: /images/covers/codeception-logo.svg
license:
- by
- nc
- nd
language:
- ru
- en
---

Писать BDD тесты на фреймворке codeception довольно удобно. Фреймворк довольно взрослый и стабильный.

Будем покрывать тестами этот сайт.

* Создание Docker образа
* Проверка API

## Что мы должны иметь?

* ОС macOS или linux/amd64
* установленный docker
* какой-нибудь удобный редактор

Создайте проект ``mycodeception``

## Создание Docker образа с Codeception

### Создаем файл Dockerfile в корне проекта
```dockerfile[Dockerfile]
FROM php:8.1-cli

WORKDIR /home/codecept

RUN apt-get update \
    && apt-get install -y wget libzip-dev zlib1g-dev \
    && CFLAGS="$CFLAGS -D_GNU_SOURCE" docker-php-ext-install sockets zip \
    && wget https://getcomposer.org/download/latest-stable/composer.phar \
    && chmod +x composer.phar \
    && mv composer.phar /usr/local/bin/composer \
    && php /usr/local/bin/composer require "codeception/codeception"

RUN useradd codecept -d /home/codecept -s /bin/bash && chown -R codecept.codecept .

USER codecept
```
### Соберем образ

```bash
docker build -t mycodeception/php8.1-cli:latest .
```

Проверим, что образ успешно создан
```bash
docker images | grep mycodeception
```

## Инициализация Codeception

Создайте файл ``.env.local`` в корне проекта
```env[.env.local]
API_URL=https://devnet.kz/api-content
SITE_URL=https://devnet.kz
```

Создайте папку ``src`` в корне проекта, и можно запустить docker образ из корня проекта для загрузки зависимостей
```bash
docker run -it --rm --name mycodeception -v "$(PWD)/src:/home/codecept" --env-file .env.local mycodeception/php8.1-cli:latest composer install
```

Теперь можно инициализировать ``codeception`` фреймворк (пока только API suite), если в терминале вас спросят об установке ``module-asserts`` и другие вещи по-умолчанию - подтвердите и оставьте по-умолчанию (простите за тавтологию).
Тем самым будет создана папка ``tests``
```bash
docker run -it --rm --name mycodeception -v "$(PWD)/src:/home/codecept" --env-file .env.local mycodeception/php8.1-cli:latest php vendor/bin/codecept init api
```

Содержание будет такое
```bash
ls -la tests
total 4
drwxr-xr-x 6 codecept codecept 192 Mar 22 13:03 .
drwxr-xr-x 9 root     root     288 Mar 22 13:03 ..
-rw-r--r-- 1 codecept codecept 176 Mar 22 13:03 ApiCest.php
drwxr-xr-x 3 codecept codecept  96 Mar 22 13:03 _data
drwxr-xr-x 3 codecept codecept  96 Mar 22 13:03 _output
drwxr-xr-x 5 codecept codecept 160 Mar 22 13:03 _support
```

## Первый тест на Codeception

Приведите файл ``codeception.yml`` к виду 
```yaml[codeception.yml]
# suite config
suites:
  api:
    actor: ApiTester
    path:  .
    modules:
      enabled:
        - REST:
            url:     '%API_URL%'
            depends: PhpBrowser
        - PhpBrowser:
            url: '%API_URL%'
            headers:
              Content-Type: application/json
    step_decorators:
      - \Codeception\Step\AsJson
    config:
      PhpBrowser:
        headers:
          Content-Type: application/json

paths:
  tests:   tests
  output:  tests/_output
  data:    tests/_data
  support: tests/_support

settings:
  shuffle: false
  lint:    true

params:
  - env
```

В файле ``ApiCest.php`` уже можно начать писать тесты, первое, что мы должны сделать - получить список статей на сайте,
по-умолчанию там будет такое содержание
```php[ApiCest.php]
<?php
class ApiCest 
{    
    public function tryApi(ApiTester $I)
    {
        $I->sendGet('/');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }
}
```

Нужно будет привести его в такой вид

```php[ApiCest.php]
<?php

use Codeception\Scenario;

class ApiCest
{

    public function tryGetIndexAndSettings(ApiTester $I, Scenario $scenario)
    {
        $body = [
            "deep"   => true,
            "text"   => false,
            "sortBy" => [
                [
                    "createdAt" => "desc",
                ],
            ],
            "only"   => [
                "title",
                "path",
                "createdAt",
            ],
        ];

        $I->sendPost('/ru', $body);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }
}

```

Запустите в терминале первый тест
```bash
docker run -it --rm --name mycodeception -v "$(PWD)/src:/home/codecept" --env-file .env.local mycodeception/php8.1-cli:latest php vendor/bin/codecept run --steps --debug

```

Результатом выполнения будет примерно такое
```bash
Codeception PHP Testing Framework v4.1.31
Powered by PHPUnit 9.5.19

Api Tests (1) -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Modules: REST, PhpBrowser
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ApiCest: Try get index and settings
Signature: ApiCest:tryGetIndexAndSettings
Test: tests/ApiCest.php:tryGetIndexAndSettings
Scenario --
 I send post "/ru",{"deep":true,"text":false,"sortBy":[{"createdAt":"desc"}],"only":["title","path","createdAt"]}
  [Request] POST https://devnet.kz/api-content/ru {"deep":true,"text":false,"sortBy":[{"createdAt":"desc"}],"only":["title","path","createdAt"]}
  [Request Headers] {"Content-Type":"application/json"}
  [Page] https://devnet.kz/api-content/ru
  [Response] 200
  [Request Cookies] []
  [Response Headers] {"x-dns-prefetch-control":["off"],"expect-ct":["max-age=0"],"x-frame-options":["SAMEORIGIN"],"strict-transport-security":["max-age=15552000; includeSubDomains"],"x-download-options":["noopen"],"x-content-type-options":["nosniff"],"x-permitted-cross-domain-policies":["none"],"referrer-policy":["no-referrer"],"x-xss-protection":["0"],"etag":[""dd-d65md36b9pu3HiFfGPQr0nPeips""],"content-type":["application/json; charset=utf-8"],"content-length":["221"],"vary":["Accept-Encoding"],"date":["Mon, 04 Apr 2022 04:41:13 GMT"],"keep-alive":["timeout=5"],"Content-Type":["text/html"]}
  [Response] [{"title":"Запуск BDD тестов через фреймворк Codeception из Docker контейнера (часть 1)","createdAt":"2022-03-27T00:00:00.000Z","path":"/ru/2022/03/php-codeception-with-docker"}]
 I see response code is 200
 I see response is json 
 PASSED 

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


Time: 00:00.812, Memory: 10.00 MB

OK (1 test, 3 assertions)

```

Ссылка на репозиторий [https://gitlab.com/devnet.kz/site-codeception-tests](https://gitlab.com/devnet.kz/site-codeception-tests)

Там же найдете Makefile с полезными командами.

**Во второй части будем расширять тесты**

* порядок выполнения тестов
* fixtures

Также затронем CI/CD Gitlab - встроим этот тест в pipeline.

По всем вопросам/уточнениям на почту.
