export default {
    name:        "Devnet",
    description: "Articles for developers, QA and Devops engineers",

    layout: {
        header:     {
            darkModeOn:  "Dark",
            darkModeOff: "Light",
        },
        footer:     {
            copyright: "Devnet.kz",
            columns:   {
                opensource: "Opensource projects",
                other:      "Other sections",
            },
        },
        toMainPage: "to main",
        or:         "or",
        back:       "back",
    },

    label: {
        readMore:            "read more",
        readingTime:         "0 minutes | {count} minute | {count} minutes",
        readingTimeExtended: "Time to read ~ 0 minutes |Time to read ~ {n} minute |Time to read ~ {n} minutes",
        wordCount:           "0 words | {count} word | {count} words",
        updatedAt:           "Updated at: {updatedAt}",
        createdAt:           "Created at: {createdAt}",
        licenseCc:           "Creative Commons license",
        linkedin:            "Linkedin profile",
        gitlab:              "Gitlab page",
        github:              "Github page",
        patreon:             "Become a sponsor!",
        edit:                "Edit",
        docker:              "Docker hub",
        telegram:            "Telegram channel",
        youtube:             "Youtube channel",
        facebook:            "Facebook",
        tags:                "Tags",
        authors:             "Creators",
        buymeacoffee:        "Buy me a coffee!",
        filter:              {
            document: "filter",
        },
        search:              {
            document: "search",
        },
        locales:             {
            ru: "Русский",
            en: "English",
            kk: "Қазақ",
        }
    },

    error: {
        notFound:               "not found, sorry dude",
        other:                  "something went wrong, sorry dude, admin notified about that",
        minScreen:              "too tight!",
        articlePendingApproval: "Article pending approval",
    },

    page: {
        about: {
            description: "About site",
        },
    },
}
