export default {
    name:        "Devnet",
    description: "Статьи для разработчиков, тестировщиков и devops",

    layout: {
        header:     {
            darkModeOn:  "Тёмная",
            darkModeOff: "Светлая",
        },
        footer:     {
            copyright: "Devnet.kz",
            columns:   {
                opensource: "Opensource проекты",
                other:      "Другие разделы сайты",
            },
        },
        toMainPage: "на главную",
        or:         "или",
        back:       "назад",
    },

    label: {
        readMore:            "читать",
        readingTime:         "0 минут | {n} минута | {n} минуты | {n} минут",
        readingTimeExtended: "Время на прочтение ~ 0 минут |Время на прочтение ~ {n} минута |Время на прочтение ~ {n} минуты |Время на прочтение ~ {n} минут",
        wordCount:           "0 слов | {n} слово | {n} слова | {n} слов",
        updatedAt:           "Обновлено: {updatedAt}",
        createdAt:           "Создано: {createdAt}",
        licenseCc:           "Лицензия Creative Commons",
        linkedin:            "Профиль на Linkedin",
        gitlab:              "Страница на Gitlab",
        github:              "Страница на Github",
        patreon:             "Стать спонсором!",
        edit:                "Редактировать",
        docker:              "Docker hub",
        telegram:            "Telegram канал",
        youtube:             "Youtube канал",
        facebook:            "Facebook",
        tags:                "Теги",
        authors:             "Авторы",
        buymeacoffee:        "Купите мне кофе!",
        filter:              {
            document: "фильтровать",
        },
        search:              {
            document: "искать",
        },
        locales:             {
            ru: "Русский",
            en: "English",
            kk: "Қазақ",
        }
    },

    error: {
        notFound:               "не найдено, прости чувак",
        other:                  "что-то пошло не так, прости чувак, админы уже уведомлены об этом",
        minScreen:              "слишком узко!",
        articlePendingApproval: "Статья ожидает подтверждения",
    },

    page: {
        about: {
            description: "О сайте",
        },
    },
}
