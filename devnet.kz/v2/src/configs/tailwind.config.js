export default {
    content:  [
        './src/components/**/*.{vue,js}',
        './src/layouts/**/*.vue',
        './src/pages/**/*.vue',
        './src/plugins/**/*.{js,ts}',
        'nuxt.config.{js,ts}'
    ],
    mode:     'jit',
    darkMode: 'class', // or 'media' or 'class'
    theme:    {
        screens: {
            'mobile': '320px',
            // => @media (min-width: 640px) { ... }
            'tablet': '640px',
            // => @media (min-width: 640px) { ... }
            'laptop': '1024px',
            // => @media (min-width: 1024px) { ... }
            'desktop': '1280px',
            // => @media (min-width: 1280px) { ... }
            'xs': '320px',
            // => @media (min-width: 640px) { ... }
            'sm': '640px',
            // => @media (min-width: 640px) { ... }
            'md': '768px',
            // => @media (min-width: 768px) { ... }
            'lg': '1024px',
            // => @media (min-width: 1024px) { ... }
            'xl': '1280px',
            // => @media (min-width: 1280px) { ... }
            '2xl': '1536px',
            // => @media (min-width: 1536px) { ... }
        },
        extend:  {
            backdropBlur: {
                xs: '2px',
            },
        },
    },
    variants: {
        extend: {},
    },
    plugins:  [],
}
