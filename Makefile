.SILENT:
.PHONY:

v2_compose_file      = docker-compose-v2.yml
v3_compose_file      = docker-compose-v3.yml
v4_compose_file      = docker-compose-v4.yml
v2_container_nuxt    = v2-nuxt
v2_container_nuxt    = v2-nuxt
v3_container_nuxt    = v3-nuxt
v4_container_nuxt    = v4-nuxt
v4_container_mongodb = v4-mongodb
env_file_v2          = devnet.kz/v2/.env.local
env_file_v3          = devnet.kz/v3/.env.local
env_file_v4          = devnet.kz/v4/.env.local

up-v2:
	docker compose -f $(v2_compose_file) up -d

up-v3:
	docker compose -f $(v3_compose_file) up -d

up-v4:
	docker compose -f $(v4_compose_file) up -d

down:
	docker compose -f $(v2_compose_file) -f $(v3_compose_file) -f $(v4_compose_file) down --remove-orphans

bash-v2:
	docker compose -f $(v2_compose_file) exec $(v2_container_nuxt) bash

bash-v3:
	docker compose -f $(v3_compose_file) exec $(v3_container_nuxt) bash

bash-v4:
	docker compose -f $(v4_compose_file) exec $(v4_container_nuxt) bash

install-dependencies-v2:
	docker compose -f $(v2_compose_file) exec -t $(v2_container_nuxt) yarn install

install-dependencies-v3:
	docker compose -f $(v3_compose_file) exec -t $(v3_container_nuxt) yarn install

install-dependencies-v4:
	docker compose -f $(v4_compose_file) exec -t $(v4_container_nuxt) yarn install

start-nuxt-server-v2:
	docker compose -f $(v2_compose_file) exec -it $(v2_container_nuxt) node_modules/.bin/nuxt

start-nuxt-server-v3:
	docker compose -f $(v3_compose_file) exec -it $(v3_container_nuxt) npx nuxi dev --dotenv $(env_file_v3)

start-nuxt-server-v4:
	docker compose -f $(v4_compose_file) exec -it $(v4_container_nuxt) npx nuxi dev --dotenv $(env_file_v4)

start-v2: up-v2 start-nuxt-server-v2
start-v3: up-v3 start-nuxt-server-v3
start-v4: up-v4 start-nuxt-server-v4

init-v2: up-v2 install-dependencies-v2 start-nuxt-server-v2
init-v3: up-v3 install-dependencies-v3 start-nuxt-server-v3
init-v4: up-v4 install-dependencies-v4 start-nuxt-server-v4

init: init-v2 init-v3 init-v4

.DEFAULT_GOAL := init
